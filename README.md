# New Framework for the Analysis of Aquatic Ecosystems
This project is part of the [NFDI4Earth](https://www.nfdi4earth.de/).

## Project goals
- New analysis pipeline that extends species-based classification 
to quantify relevant functional traits from images. 

- Make machine learning approaches to image analysis accessible
to domain specialists without programming expertise. 

- Evaluate performance (accuracy) to choose between machine learning paradigms. such as label-free, unsupervised 

- Tools must be deployable at scale on modern high performance computing (HPC) systems. 

This project includes at the following files to ensure a FAIR repository management:
- README.md
    - Structured description (see How to install below)
    - DOI Batch
- LICENSE
- CHANGELOG.md
- CITATION.cff
- [project_description.pdf](project-description.pdf)
- [Documentation](documentation/Plankton-classifier-analysis-of-Phytodive-CPICS-images.md) of use of data analysis pipeline
- a tutorial.ipynb will be made available upon publication of the work.

Videos containing a scientific talk and a tutorial will be provided via the NFDI4Earth YouTube channel and will be linked in this README.md upon publication of this work. [Video](https://youtu.be/X8ZUOg0v8iQ)

## How to install
Our data analysis pipeline implements an existing, open-source labeling platform [Label-Studio](https://labelstud.io) on an HPC system.
We recommend storing the data, implementing the labeling platform and installing the CNN on an HPC system for scalable deployment. To use the data analysis pipeline you will need
to install Label-studio and the convolutional neural network(s) (CNN) of your choice on your HPC system or computer. Instructions to install our Plankton-classifer CNN are also provided here, the link to the repo will be made public upon publication.

### Install Label-studio on an HPC system:
Follow the [regular installation guide](https://labelstud.io/guide/install.html) to install on a local machine. To install on an HPC system:
1. Make sure you have ssh key-based access to your HPC system and can set up ssh tunnels.
2. Make a new conda environment or virtualenv. Check that it's using a python 3.9 executable
3. `pip install label-studio`
4. Start label-studio for the first time, [follow instructions](https://labelstud.io/guide/index.html) to make a new project
5. Download [this script](https://github.com/heartexlabs/label-studio/blob/master/scripts/serve_local_files.sh) and use `chmod u+x` to allow exectuion of it

### Install postgresql server for label-studio: 
[PostgreSQL](https://www.postgresql.org) is an open-source database 
that can be implemented as a backend in label-studio to 
support concurrent users and a large number (millions) of 
task annotations. Installation without root/sudo rights on an 
HPC system requires compiling from source. Instruction have been adapted from [the docs](https://www.postgresql.org/files/documentation/pdf/15/postgresql-15-A4.pdf).  
1. On your HPC system, use `wget` to download the latest postgresql [server source code](https://www.postgresql.org/ftp/source/).
2. Unzip the archive e.g. `tar xf postgresql-version.tar.bz2`
3. Remove the archive and go into the newly created directory. Run `./configure  --prefix=PATH_TO_INSTALL_LOCATION` for example somewhere inside your home or work directories. Don't put a `/` on the end of this path.
4. Run `make`
5. Run `make check` and see if the all tests have passed.
6. Run `make install`
7. Run `make clean` to free up space
8. Add this line to your .bashrc file: 
`export LD_LIBRARY_PATH=/path/to/your/home/directory/pgsql/lib`. If `LD_LIBRARY_PATH` is already set, append this directory using colons `:` as seperators.
9. Add this line to your .bashrc file `export PATH="/path/to/your/home/directory/pgsql/bin:$PATH"` 
10. Add these lines to your .bashrc file:
`
export DJANGO_DB=default
export POSTGRE_NAME=postgres
export POSTGRE_USER=username
export POSTGRE_PASSWORD=
export POSTGRE_PORT=5432
export POSTGRE_HOST=localhost
`
11. Close and restart your bash etc. so new environment variables will take effect. Starting a new window in screen or tmux is generally NOT sufficient.
12. Run `initdb -D DATAPATH` where DATAPATH is where on the HPC system's filesystem you want to store data. 
13. Install the pg_trgm extension to postgresql. Go back to the directory that was unzipped from the downloaded archive, and then into the subdirectory `contrib/pg_trgm`
14. Run `make`, then run `make install`


Instuction and code to install and use the Plankton-classifier CNN
will be made available soon.



## How to use

### Use Label-Studio on an HPC system:
1. Connect to the HPC system opening ssh tunnels on ports 8889 and 8887. You can use other ports if you like. You can use a head node or a compute node.
2. Start screen or tmux on your HPC system so all your commands will perist if your connection breaks. Make a new window for each thing you have to do. 
3. start a server running to serve your data via url, using `./serve_local_files.sh /gpfs/work/USERNAME/DIRECTORY *.png files.txt 8887` Change USERNAME and DIRECTORY as appropriate, as well as the file extension. 
4. Optional: check if label-studio is serving image data over your ssh tunnel by opening http://localhost:8887/IMAGENAME.png or for an image filename in your data (adjust as needed for other extensions, other data types, etc.). You should see the image displayed in your web browser
5. Activate the label-studio environment with `conda activate label-studio`. Start label studio with `label-studio start --port 8889`
6. Open http://localhost:8889/ in a web browser to start using label studio. Create a new project or open an existing one.
7. If creating a new project, setup the labeling interface (see below).
8. If you're including this data for the first time, download `files.txt` to your local machine via `scp`, then drag and drop it into the import data interface behind the settings button in label-studio
8. Start labelling!
9. Your results will be saved automatically. To export them, use the e[]()xport function label studio's web interace. JSON-minimal or csv files are recommended formats.

### Use label-studio with postgreSQL:
1. Start the postgreSQL server
`pg_ctl -D DATAPATH -l LOGFILE start` where LOGFILE is where you want to log possible errors etc.
2. Start `serve_local_files.sh` if needed
3. Start label-studio with `label-studio start --port 8883 -db postgresql`

### Setup labelling interface:
Label-studio [templates](https://labelstud.io/templates/gallery_cv.html) can be customized to create the labeling interface for a project.
Alternatively, XML-like tags can be used to setup the interface. More on [tags](https://labelstud.io/tags/) supported in label-studio. 
The labelling interfaces used in our work are documented in this [document](documentation/labeling_interface_templates.md)

### Use Jupyter notebook based tools to format, filter, update and split training data
We provide some useful code that can serve as a go-between using csv files to export labels out of label-studio and into a CNN. This is intended to support users of label-studio that do not wish to integrate a machine-learning backend into label-studio.
Our notebooks are [here](notebooks)

This work has been funded by the German Research Foundation (NFDI4Earth, DFG project no. 460036893, https://www.nfdi4earth.de/).