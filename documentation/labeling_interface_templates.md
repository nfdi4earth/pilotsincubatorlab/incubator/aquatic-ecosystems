# Labeling interfaces for Class and Trait annotation

Labeling in label-studio is carried out in the context of the project. Label-studio [templates](https://labelstud.io/templates/gallery_cv.html) can be customized to create the labeling interface for each project according to requirements.
Alternatively, XML-like [tags](https://labelstud.io/tags/) can be used to setup the interface. The labelling interfaces used in our work are documented in this notebook. To use these templates, copy the code into the labeling interface field in your label-studio project settings.

### Simple, single choice class annotation

```
<View>
   <Image name="image" value="$image" />
    <Choices name="Class" toName="image">
    	<Choice value="Amphipods"/>
     	 <Choice value="Blurry"/>
      	<Choice value="Copepods"/>
      	<Choice value="Cumacea"/>
      	<Choice value="Marine snow"/>
      	<Choice value="Other"/>
      	<Choice value="Polychaetes"/>
        <Choice value="Polychaetes with gonads"/>
  </Choices>
</View>
```

### Multiple choice class annotation
Add `` choice="multiple" `` when declaring the ``Choices_name``. Like so:
```<View>
   <Image name="image" value="$image" />
    <Choices name="Class" toName="image" choice="multiple"> #name your choice
    	<Choice value="Amphipods"/>
     	 <Choice value="Blurry"/>
      	<Choice value="Copepods"/>
      	<Choice value="Cumacea"/>
      	<Choice value="Marine snow"/>
      	<Choice value="Other"/>
      	<Choice value="Polychaetes"/>
        <Choice value="Polychaetes with gonads"/> # replace with relevant choices
  </Choices>
</View>
```
### Add Keypoint labels to image
Can be useful to add reference point(s) in the image that can be used to approximately derive orientation, size or define points in the image.
#%% md
Add the following lines to the interface code:
#%% md
 ```<KeyPointLabels name="kp-1" toName="image">
    <Label value="Head"/> #replace with relevant names
    <Label value="Tail"/>
  </KeyPointLabels>
  ```

### Adjust display

#### Split the display
Can be useful when displaying a lot of options for labeling esp. to ensure images are not displayed too small. Use ``<View style="display: flex;"> `` followed by `` <View style="flex: x%"> and <View style="flex: (100-x)%;  margin-left: 1em"> for the two display regions. For example:
#%% md
```<View style="display: flex;">
  <View style="flex: 50%">
   <Image name="image" value="$image"/>
  <KeyPointLabels name="kp-1" toName="image">
    <Label value="Head"/>
    <Label value="Tail"/>
  </KeyPointLabels>
    <Choices name="Class" toName="image">
    <Header value="Single organism:">
      <Choice value="Diatom chain"/>
          <Choice value="Noctiluca"/>
        <Choice value="Marine snow"/>
          <Choice value="Blurry"/>
      <Choice value="Pluteus larvae"/>
      <Choice value="Ctenophore"/>
          <Choice value="Copepods"/>
          <Choice value="Other"/>
  <Choice value="Bubbles"/>
<Choice value="Dinoflagellate"/></Choices>
</View>
  <View style="flex: 50%; margin-left: 1em">
    <Header value="Multiple organisms:"/>
     <Choices name="Co-occurence" toName="image" choice="multiple">
      <Choice value="Diatom chain"/>
          <Choice value="Noctiluca"/>
          <Choice value="Dinoflagellate"/>
        <Choice value="Marine snow"/>
          <Choice value="Eggs"/>
          <Choice value="Copepods"/>
       <Choice value="Pluteus larvae"/>
          <Choice value="Other"/>
  </Choices>
</View>
  </View>
  ```

#### Adjust image rendering
Sometimes it is helpful to display an image in its default size (too much zoom can cause pixelation). For this use:
`` defaultZoom  = "original" zoomControl ="true"`` to render the image.
If blowing up the image to fill up the screen is desired, use ``zoom="true" zoomControl="true" maxWidth="100%"/`` to render the image.
Example:
#%% md
```<View style="display: flex;">
  <View style="flex: 50%">
   <Image name="image" value="$image" zoom="true" zoomControl="true" maxWidth="100%"/>
        <View>
    <Text name="prediction" value="$prediction"/>
  </View>
    <Header value="CNN prediction:"/>
    <Choices name="cnn-eval" toName="image">
    <Choice value="Right"/>
    <Choice value="Wrong"/>
    <Choice value="Ambiguous"/></Choices>
  </View>
   <View style="flex: 50%; margin-left: 1em">
   <Header value="Class Annotation:"/>
      <Choices name="class" toName="image">
      <Choice value="Diatom chain"/>
          <Choice value="Noctiluca"/>
        <Choice value="Marine snow"/>
          <Choice value="Blurry"/>
      <Choice value="Pluteus larvae"/>
      <Choice value="Ctenophore"/>
          <Choice value="Copepods"/>
          <Choice value="Other"/>
  <Choice value="Bubbles"/>
   <Choice value="Dinoflagellate"/></Choices>
</View>
</View>
```

#### Split annotations over columns to fit display
When displaying many annotation options, options can be distributed over the desired number of columns by `` <Style> .columns-bal {column-count: 3; column-fill: balance; }</Style>`` Example:

```<View style="display: flex;">
  <View style="flex: 25%">
  <Image name="image" value="$image" defaultZoom  = "original" zoomControl ="true"/>
 </View>
    <View style="flex: 75%; margin-left: 1em">
  <Style> .columns-bal {column-count: 3; column-fill: balance; }</Style> #set the number of column in column count

    ```<View className="columns-bal">
    <Choices name="choice" toName="image">
      <Choice value="Blurry"/>
      <Choice value="Microphytoplankon"/>
      <Choice value="Non-living particles"/>
      <Choice value="Other living organisms"/>
      <Choice value="Small centric diatoms"/>
      <Choice value="Large centric diatoms"/>
      <Choice value="Pennate diatoms"/>
      <Choice value="Diatom: Odontella"/>
      <Choice value="Diatom: Chaetoceros"/>
      <Choice value="Diverse microzooplankton"/>
      <Choice value="Dinoflagellates"/>
      <Choice value="Dinoflagellate: Ceratium"/>
      <Choice value="Dinoflagellate: Dinophysis"/>
      <Choice value="Dinoflagellate: Noctiluca"/>
      <Choice value="Radiolaria"/>
      <Choice value="Tintinnina"/>
      <Choice value="Marine snow"/></Choices>
    </View>
   </View>
</View>
```

#### Display text-fields along with an image
Can be useful to view confidence estimates reported for a CNN prediction along with the image. Use ``<View>
          <Text name="max_p" value="$maxp"/>
  </View>`` Example

```<View style="display: flex;">
  <View style="flex: 75%">
   <Image name="image" value="$image" zoom="true" zoomControl="true" maxWidth="100%"/>
        <View>
          <Text name="max_p" value="$maxp"/>
  </View>

    <Style> .columns-bal {column-count: 3; column-fill: balance; }</Style>

    <View className="columns-bal">
    <Header value="Behavior:"/>
    <Choices name="traits" toName="image" choice="multiple">
    <Choice value="Agglomeration"/>
    <Choice value="Phagocytosis"/>
    <Choice value="Ingestion/Egestion"/>
      <Choice value="Bright nuclues"/>
      <Choice value="Dividing cell"/>
      </Choices>
      <Choices name="Phagocytosis" toName="image" choice="multiple">
      <Header value="Prey:"/>
      <Choice value="Diatom chain"/>
      <Choice value="Marine snow"/>
      <Choice value="Copepod"/>
      <Choice value="Eggs"/>
      <Choice value="Larvae"/>
      <Choice value="Multiple"/>
        </Choices>
        <Choices name="Co-occurence" toName="image" choice="multiple">
      <Header value="Co-occurence:"/>
      <Choice value="Diatom chain"/>
      <Choice value="Marine snow"/>
      <Choice value="Copepod"/>
      <Choice value="Eggs"/>
      <Choice value="Larvae"/>
      <Choice value="Multiple"/>
    </Choices>
     </View>
  </View>
   <View style="flex: 25%; margin-left: 1em">
   <Header value="Class Annotation:"/>
      <Choices name="class" toName="image">
      <Choice value="Diatom chain"/>
          <Choice value="Noctiluca"/>
        <Choice value="Marine snow"/>
          <Choice value="Blurry"/>
      <Choice value="Pluteus larvae"/>
      <Choice value="Ctenophore"/>
          <Choice value="Copepods"/>
          <Choice value="Other"/>
  <Choice value="Bubbles"/>
   <Choice value="Dinoflagellate"/>
     <Choice value="Jelly"/>
        <Choice value="Phoronida"/>
        <Choice value="Chaetognates"/>
        <Choice value="Appendicularia"/><Choice value="Malacostraca"/><Choice value="Acrania"/><Choice value="Cirripedia"/><Choice value="Cladocera"/></Choices>
</View>
</View>
```